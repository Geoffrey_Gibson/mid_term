﻿CREATE TABLE [dbo].[Jobs] (
    [Job_No]      UNIQUEIDENTIFIER NOT NULL,
    [Job_Name]    VARCHAR (20)     NOT NULL,
    [Job_Details] VARCHAR (200)    NOT NULL,
    [Hourly_Wage] DECIMAL (3, 2)   NOT NULL,
    [Start_Date]  DATETIME         NOT NULL,
    [End_Date]    DATETIME         NOT NULL,
    [Completed]   BIT              NOT NULL,
    [Emp_No]      INT              NOT NULL,
    [Task_No]     UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([Job_No] ASC),
    FOREIGN KEY ([Emp_No]) REFERENCES [dbo].[Employees] ([Emp_No]) ON DELETE CASCADE,
    FOREIGN KEY ([Task_No]) REFERENCES [dbo].[Tasks] ([Task_No]) ON DELETE CASCADE
);

