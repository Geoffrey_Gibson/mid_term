﻿CREATE TABLE [dbo].[Tasks] (
    [Task_No]        UNIQUEIDENTIFIER NOT NULL,
    [Task_Name]      VARCHAR (24)     NOT NULL,
    [Task_Details]   VARCHAR (30)     NOT NULL,
    [Estimated_Time] DATETIME         NOT NULL,
    [Completed]      BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Task_No] ASC)
);

