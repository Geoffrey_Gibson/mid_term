﻿CREATE TABLE [dbo].[Employees] (
    [Emp_No]          INT           NOT NULL,
    [First_Name]      VARCHAR (24)  NOT NULL,
    [Last_Name]       VARCHAR (30)  NOT NULL,
    [Title]           VARCHAR (3)   NOT NULL,
    [Phone_No]        VARCHAR (14)  NOT NULL,
    [Email]           VARCHAR (100) NOT NULL,
    [Street_Address1] VARCHAR (100) NOT NULL,
    [Street_Address2] VARCHAR (10)  NOT NULL,
    [City]            VARCHAR (30)  NOT NULL,
    [State]           CHAR (2)      NOT NULL,
    [Zip_Code]        VARCHAR (10)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Emp_No] ASC)
);

