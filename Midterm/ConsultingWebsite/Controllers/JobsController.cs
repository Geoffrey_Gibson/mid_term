﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConsultingWebsite.Models;
using ConsultingWebsite.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ConsultingWebsite.Controllers
{
    [Authorize]
    public class JobsController : Controller
    {
        private ConsultingDbContext db = new ConsultingDbContext();

        // GET: Jobs
        public ActionResult Index(string search)
        {
            bool isManager = User.IsInRole("Manager");
            bool isAdmin = User.IsInRole("Administrator");

            var jobs = db.Jobs.Include(j => j.Employee).Include(j => j.Tasks);

            var jobInfo =
                from j in jobs
                orderby j.Job_No
                select new JobInfo
                {
                    Job_No = j.Job_No,
                    Job_Name = j.Job_Name,
                    Job_Details = j.Job_Details,
                    Hourly_Wage = j.Hourly_Wage,
                    Start_Date = j.Start_Date,
                    End_Date = j.End_Date,
                    Completed = j.Completed,
                    Emp_No = j.Emp_No
                };

            var userId = HttpContext.User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindById(userId);

            if (!isAdmin || !isManager)
            {
                var employee = (
                    from e in db.Employees
                    where e.Emp_No == user.emp_no
                    select e.Emp_No).FirstOrDefault();

                jobInfo = jobInfo.Where(x => x.Emp_No == employee);
            }
            if (!string.IsNullOrEmpty(search))
            {
                jobInfo = jobInfo.Where(j => j.Job_Name.Contains(search));
            }

            ViewBag.Search = search;

            return View(jobs.ToList());
        }

        // GET: Jobs/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // GET: Jobs/Create
        public ActionResult Create()
        {
            ViewBag.Emp_No = new SelectList(db.Employees, "Emp_No", "First_Name");
            ViewBag.Job_No = new SelectList(db.Tasks, "Task_No", "Task_Name");
            return View();
        }

        // POST: Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Manager,Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Job_No,Job_Name,Job_Details,Hourly_Wage,Start_Date,End_Date,Completed,Emp_No,Task_No")] Job job)
        {
            if (ModelState.IsValid)
            {
                job.Job_No = Guid.NewGuid();
                db.Jobs.Add(job);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Emp_No = new SelectList(db.Employees, "Emp_No", "First_Name", job.Emp_No);
            ViewBag.Job_No = new SelectList(db.Tasks, "Task_No", "Task_Name", job.Job_No);
            return View(job);
        }

        // GET: Jobs/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            ViewBag.Emp_No = new SelectList(db.Employees, "Emp_No", "First_Name", job.Emp_No);
            ViewBag.Job_No = new SelectList(db.Tasks, "Task_No", "Task_Name", job.Job_No);
            return View(job);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Manager,Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Job_No,Job_Name,Job_Details,Hourly_Wage,Start_Date,End_Date,Completed,Emp_No,Task_No")] Job job)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Emp_No = new SelectList(db.Employees, "Emp_No", "First_Name", job.Emp_No);
            ViewBag.Job_No = new SelectList(db.Tasks, "Task_No", "Task_Name", job.Job_No);
            return View(job);
        }

        // GET: Jobs/Delete/5
        [Authorize(Roles = "Manager,Administrator")]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Job job = db.Jobs.Find(id);
            db.Jobs.Remove(job);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
