﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConsultingWebsite.Models;

namespace ConsultingWebsite.Controllers
{
    public class HoursController : Controller
    {
        private ConsultingDbContext db = new ConsultingDbContext();

        // GET: Hours
        public ActionResult Index()
        {
            var hours = db.Hours.Include(h => h.Job).Include(h => h.Task);
            return View(hours.ToList());
        }

        // GET: Hours/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hour hour = db.Hours.Find(id);
            if (hour == null)
            {
                return HttpNotFound();
            }
            return View(hour);
        }

        // GET: Hours/Create
        public ActionResult Create()
        {
            ViewBag.Job_No = new SelectList(db.Jobs, "Job_No", "Job_Name");
            ViewBag.Task_No = new SelectList(db.Tasks, "Task_No", "Task_Name");
            return View();
        }

        // POST: Hours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Job_No,Task_No,Date,Hours_Worked")] Hour hour)
        {
            if (ModelState.IsValid)
            {
                hour.Job_No = Guid.NewGuid();
                db.Hours.Add(hour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Job_No = new SelectList(db.Jobs, "Job_No", "Job_Name", hour.Job_No);
            ViewBag.Task_No = new SelectList(db.Tasks, "Task_No", "Task_Name", hour.Task_No);
            return View(hour);
        }

        // GET: Hours/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hour hour = db.Hours.Find(id);
            if (hour == null)
            {
                return HttpNotFound();
            }
            ViewBag.Job_No = new SelectList(db.Jobs, "Job_No", "Job_Name", hour.Job_No);
            ViewBag.Task_No = new SelectList(db.Tasks, "Task_No", "Task_Name", hour.Task_No);
            return View(hour);
        }

        // POST: Hours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Job_No,Task_No,Date,Hours_Worked")] Hour hour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Job_No = new SelectList(db.Jobs, "Job_No", "Job_Name", hour.Job_No);
            ViewBag.Task_No = new SelectList(db.Tasks, "Task_No", "Task_Name", hour.Task_No);
            return View(hour);
        }

        // GET: Hours/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hour hour = db.Hours.Find(id);
            if (hour == null)
            {
                return HttpNotFound();
            }
            return View(hour);
        }

        // POST: Hours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Hour hour = db.Hours.Find(id);
            db.Hours.Remove(hour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
