﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConsultingWebsite.Models;
using ConsultingWebsite.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;

namespace ConsultingWebsite.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {
        private ConsultingDbContext db = new ConsultingDbContext();

        // GET: Employees
        public ActionResult Index(
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            var emps =
                from e in db.Employees
                orderby e.Emp_No
                select new EmployeeInfo
                {
                    Emp_No = e.Emp_No,
                    First_Name = e.First_Name,
                    Last_Name = e.Last_Name,
                    Title = e.Title,
                    Phone_No = e.Phone_No,
                    Email = e.Email,
                    Street_Address1 = e.Street_Address1,
                    Street_Address2 = e.Street_Address2
                };


            if (!User.IsInRole("Administrator") || !User.IsInRole("HR"))
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = userManager.FindById(userId);

                var employee = (
                    from e in db.Employees
                    where e.Emp_No == user.emp_no
                    select e.Emp_No).FirstOrDefault();

                emps = emps.Where(x => x.Emp_No == employee);
            }
            if (!string.IsNullOrEmpty(search))
            {
                emps = emps.Where(x => x.Last_Name.Contains(search) || x.First_Name.Contains(search));
            }

            switch (sortBy)
            {
                default:
                    emps = emps.OrderBy(e => e.Emp_No);
                    break;
                case "Employee #":
                    emps = emps.OrderBy(e => e.Emp_No);
                    break;
                case "Last Name":
                    emps = emps.OrderBy(e => e.Last_Name);
                    break;
                case "First Name":
                    emps = emps.OrderBy(e => e.First_Name);
                    break;
            }

            var model = new EmployeeSearchResults
            {
                Search = search,
                SortBy = sortBy,
                Results = emps.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            ViewBag.Search = search;

            return View("Index", model);
        }

        // GET: Employees/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindById(userId);

            Employee loggedInUser = db.Employees.FirstOrDefault(x => x.Emp_No == user.emp_no);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(loggedInUser);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "HR,Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Emp_No,First_Name,Last_Name,Title,Phone_No,Email,Street_Address1,Street_Address2,City,State,Zip_Code")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Emp_No,First_Name,Last_Name,Title,Phone_No,Email,Street_Address1,Street_Address2,City,State,Zip_Code")] Employee employee)
        {
            if (!User.IsInRole("Administrator") && !User.IsInRole("HR"))
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = userManager.FindById(userId);

                Employee loggedInUser = db.Employees.FirstOrDefault(x => x.Emp_No == user.emp_no);

                if (ModelState.IsValid)
                {
                    db.Entry(loggedInUser).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(loggedInUser);
            }
            else if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        [Authorize(Roles = "Administrator,HR")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "HR,Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
