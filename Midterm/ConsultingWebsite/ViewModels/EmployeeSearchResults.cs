﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.ViewModels
{
    public class EmployeeSearchResults
    {
        public string Search { get; set; }
        public string SortBy { get; set; }
        public IPagedList<EmployeeInfo> Results { get; set; }
    }
}