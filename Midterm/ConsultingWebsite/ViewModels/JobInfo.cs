﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.ViewModels
{
    public class JobInfo
    {
        public Guid Job_No { get; set; }
        
        public string Job_Name { get; set; }
        
        public string Job_Details { get; set; }
        
        public decimal Hourly_Wage { get; set; }
        
        public DateTime Start_Date { get; set; }
        
        public DateTime End_Date { get; set; }
        
        public bool Completed { get; set; }
       
        public int Emp_No { get; set; }
    }
}