﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.ViewModels
{
    public class EmployeeInfo
    {
        [Display(Name = "Employee #")]
        public int Emp_No { get; set; }

        [Display(Name = "First Name")]
        public string First_Name { get; set; }

        [Display(Name = "Last Name")]
        public string Last_Name { get; set; }
        
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Phone #")]
        public string Phone_No { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Street Address 1")]
        public string Street_Address1 { get; set; }

        [Display(Name = "Street Address 2")]
        public string Street_Address2 { get; set; }

        public string Full_Name
        {
            get { return First_Name + Last_Name; }
        }

        public string Mailing_Address
        {
            get { return Street_Address1 + Street_Address2; }
        }
    }
}