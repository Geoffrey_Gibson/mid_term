﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConsultingWebsite.Startup))]
namespace ConsultingWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
