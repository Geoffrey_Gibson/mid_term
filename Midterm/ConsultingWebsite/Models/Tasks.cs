﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.Models
{
    public class Tasks
    {
        [Key]
        public Guid Task_No { get; set; }

        [Display(Name = "Task")]
        [MaxLength(24, ErrorMessage = "Task name is too long")]
        public string Task_Name { get; set; }

        [Display(Name = "Task Details")]
        [DataType(DataType.MultilineText)]
        [MaxLength(30, ErrorMessage = "Task details are too long")]
        public string Task_Details { get; set; }

        [Display(Name = "Estimate Time")]
        [DataType(DataType.DateTime)]
        public DateTime Estimated_Time { get; set; }

        [Display(Name = "Completed")]
        public bool Completed { get; set; }
    }
}