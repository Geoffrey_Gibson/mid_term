﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.Models
{
    public class Job
    {
        [Key]
        public Guid Job_No { get; set; }

        [Display(Name = "Job")]
        [Required]
        [MaxLength(20, ErrorMessage = "Job name is too long")]
        public string Job_Name { get; set; }

        [Display(Name = "Job Details")]
        [DataType(DataType.MultilineText)]
        [Required]
        [MaxLength(200, ErrorMessage = "Details too long")]
        public string Job_Details { get; set; }

        [Display(Name = "Hourly Wage")]
        [DataType(DataType.Currency)]
        [Required]
        public decimal Hourly_Wage { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime Start_Date { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime End_Date { get; set; }

        [Display(Name = "Completed")]
        [Required]
        public bool Completed { get; set; }

        // Foreign Keys
        public int Emp_No { get; set; }
        public Guid Task_No { get; set; }

        // Navigation
        [ForeignKey("Emp_No")]
        public virtual Employee Employee { get; set; }

        [ForeignKey("Task_No")]
        public virtual IList<Tasks> Tasks { get; set; }
    }
}