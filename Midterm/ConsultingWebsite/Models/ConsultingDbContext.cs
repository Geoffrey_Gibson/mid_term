﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.Models
{
    public class ConsultingDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Hour> Hours { get; set; }
    }
}