﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.Models
{
    public class Employee
    {
        [Key]
        public int Emp_No { get; set; }

        [Display(Name = "First Name")]
        [MaxLength(24, ErrorMessage = "Name is too long")]
        [Required]
        public string First_Name { get; set; }

        [Display(Name = "Last Name")]
        [MaxLength(30, ErrorMessage = "Name is too long")]
        [Required]
        public string Last_Name { get; set; }

        [Display(Name = "Title")]
        [MaxLength(3, ErrorMessage = "Mr/Mrs")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Phone #")]
        [DataType(DataType.PhoneNumber)]
        [Required]
        [MaxLength(14)]
        [RegularExpression(@"^(1-)?(\d{3}|\(\d{3}\) )-?\d{3}-?\d{4}$",
            ErrorMessage = "Invalid phone number")]
        public string Phone_No { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Street Address 1")]
        [Required]
        [StringLength(100)]
        [MinLength(3)]
        public string Street_Address1 { get; set; }

        [Display(Name = "Street Address 2")]
        [StringLength(10)]
        [MinLength(1)]
        public string Street_Address2 { get; set; }

        [Display(Name = "City")]
        [StringLength(30)]
        public string City { get; set; }

        [Display(Name = "State")]
        [MinLength(2)]
        public string State { get; set; }

        [Display(Name = "Zip Code")]
        [DataType(DataType.PostalCode)]
        [StringLength(10)]
        [MinLength(5)]
        [Required]
        public string Zip_Code { get; set; }

        [NotMapped]
        public string Full_Name
        {
            get { return First_Name + Last_Name; }
        }
        [NotMapped]
        public string Mailing_Address
        {
            get { return Street_Address1 + Street_Address2; }
        }

        // Navigation
        [ForeignKey("Emp_No")]
        public virtual IList<Job> Jobs { get; set; }
    }
}