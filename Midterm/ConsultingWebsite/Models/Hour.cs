﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ConsultingWebsite.Models
{
    public class Hour
    {
        [Key, Column(Order = 1)]
        public Guid Job_No { get; set; }

        [Key, Column(Order = 2)]
        public Guid Task_No { get; set; }

        // Should be current by default
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }
        public decimal Hours_Worked { get; set; }
        
        // Navigation 
        [ForeignKey("Job_No")]
        public virtual Job Job { get; set; }

        [ForeignKey("Task_No")]
        public virtual Tasks Task { get; set; }
    }
}