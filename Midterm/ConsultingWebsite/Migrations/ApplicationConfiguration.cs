namespace ConsultingWebsite.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ApplicationConfiguration : DbMigrationsConfiguration<ConsultingWebsite.Models.ApplicationDbContext>
    {
        public ApplicationConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ConsultingWebsite.Models.ApplicationDbContext";
        }

        protected override void Seed(ConsultingWebsite.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
