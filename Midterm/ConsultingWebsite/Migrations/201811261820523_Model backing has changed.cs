namespace ConsultingWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modelbackinghaschanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "emp_no", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "emp_no");
        }
    }
}
